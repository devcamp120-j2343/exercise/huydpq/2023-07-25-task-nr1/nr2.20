// khai báo thư viện express
const express = require("express");

// khai bao thu vien mongoose
var mongoose = require('mongoose');


//khai báo mongoose
const drinkSchema = require("./app/models/drinkModel");
const voucherSchema = require("./app/models/voucherModel");
const orderSchema = require("./app/models/orderModel");
const userSchema = require("./app/models/userModel");
// khai báo router

const { router } = require("./app/routes/courseRoute");
const { routerDrink } = require("./app/routes/drinkRoutes");
const routerVoucher = require("./app/routes/voucherRouter");
const routerUser = require ("./app/routes/userRoute")
const orderRouter = require("./app/routes/orderRoute")

// khỏi tại appp
const app = express();

// khỏi tại port
const port = 8000; 

// ket noi mongoose

   mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Pizza365")
  .then(() => console.log("Connected to Mongo Successfully"))
  .catch(error => handleError(error));

   

// app.use("/", (req, res, next) =>{
//     console.log(new Date());
//     next()
// }, (req, res, next) =>{
//     console.log(new Date());
//     next()
// })

// app.post("/", (req, res, next) => {
//     console.log(req.method)
//     next() 
// })

// app.get("/", (req, res) => {
//     let today = new Date();
//     res.json({
//         message: `Hom nay là ngày ${today.getDate()} tháng ${today.getMonth()} năm ${today.getFullYear()}`
//     })
// })
app.use("/",routerDrink) 
app.use("/", routerVoucher) 
app.use("/", routerUser)
app.use("/",router) 
app.use('/', orderRouter)

app.listen(port, () =>{
    console.log(`Đang chay trên cổng:  ${port}`)
})