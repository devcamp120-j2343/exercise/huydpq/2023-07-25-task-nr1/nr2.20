const { default: mongoose } = require("mongoose");
const orderModel = require("../models/orderModel")
const userModel = require("../models/userModel")
const randtoken = require('rand-token');

// Create Order
const createOrer = async (req, res) => {
    let id = req.params.userId;
    let body = req.body

    if (!body.pizzaSize) {
        return res.status(404).json({
            message: "pizzaSize is invalid"
        })
    }
    if (!body.pizzaType) {
        return res.status(404).json({
            message: "pizzaType is invalid"
        })
    }
    if (!body.status) {
        return res.status(404).json({
            message: "status is invalid"
        })
    }
    try {
        let newOder = {
            _id: new mongoose.Types.ObjectId(),
            orderCode: randtoken.generate(8),
            pizzaSize: body.pizzaSize,
            pizzaType: body.pizzaType,
            status: body.status,
        }

        const newCreateOrder = await orderModel.create(newOder);
        const updateUser = await userModel.findByIdAndUpdate(id, {
            $push: { orders: newCreateOrder._id }
        });

        return res.status(200).json({
            status: "Create Order successfully",
            user: updateUser,
            data: newCreateOrder
        })


    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        })
    }
}

// get all order

const getAllOrder = async (req, res) => {
    let id = req.query.userId;
    console.log("Get all Order")
    try {
        if (id === undefined) {
            const orderList = await orderModel.find();

            if (orderList && orderList.length > 0) {
                return res.status(200).json({
                    status: "Get all reviews sucessfully",
                    data: orderList
                })
            } else {
                return res.status(404).json({
                    status: "Not found any review"
                })
            }
        } else {
            const userInfo = await userModel.findById(id).populate("orders");

            return res.status(200).json({
                status: "Get all reviews of course sucessfully",
                data: userInfo.orders
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        })
    }
}

// get Order By ID

const getOrderByID = async (req, res) => {
    let id = req.params.orderId

    if (!mongoose.Types.ObjectId.isValid(id) && id === undefined) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }
    //B3: thực thi model
    try {
        const orderInfo = await orderModel.findById(id);

        if (orderInfo) {
            return res.status(200).json({
                status: "Get review by id sucessfully",
                data: orderInfo
            })
        } else {
            return res.status(404).json({
                status: "Not found any review"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

// update order
const updateOrder = async (req, res) => {
    let id = req.params.orderId
    let body = req.body

    if (!mongoose.Types.ObjectId.isValid(id) && id === undefined) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }

    try {
        let newOrderUpdate = {
            pizzaSize: body.pizzaSize,
            pizzaType: body.pizzaType,
            status: body.status,
        }

        const updateOrder = await orderModel.findByIdAndUpdate(id, newOrderUpdate)

        if (updateOrder) {
            return res.status(200).json({
                status: "Update Order sucessfully",
                data: updateOrder

            })
        } else {
            return res.status(404).json({
                status: "Not found any review"
            })
        }

    } catch (error) {
        return res.status(200).json({
            status: "internal Server Error",
            message: error.message
        })
    }
}

const deleteOrder = async (req, res) => {
    let id = req.params.orderId

    // Nếu muốn xóa id thừa trong mảng user thì có thể truyền thêm id để xóa
    var userId = req.query.userId;

    if (!mongoose.Types.ObjectId.isValid(id) && id === undefined) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }

    try {
        // Nếu có userId thì xóa thêm (optional)
        if (userId !== undefined) {
            await userModel.findByIdAndUpdate(userId, {
                $pull: { orders: id }
            })
        }
        const deleteOrder = await orderModel.findByIdAndDelete(id)

        if (deleteOrder) {
            return res.status(200).json({
                message: `Delete Order sucessfully`,
                deleteOrder
            })
        } else {
            return res.status(404).json({
                message: `not found any review`,

            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }

}
module.exports = { createOrer, getAllOrder, getOrderByID, updateOrder, deleteOrder }