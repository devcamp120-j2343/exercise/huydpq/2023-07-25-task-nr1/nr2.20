

const { default: mongoose } = require("mongoose")
const drinkModel = require("../models/drinkModel")

const orderModel = require("../models/orderModel")
// get all Drink
const getAllDrink = (req, res) => {
    //B1:  thu thập dữ liệu
    //B2:  kiểm tra thông tin
    // B3: xủ lý kêt quả
    console.log("Get all Drinks")
    drinkModel.find()
        .then((data) => {
            return res.status(201).json({
                message: "Get all courses sucessfully",
                courses: data
            })
        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })
}
// Get Drinks by Id
const getDrinkById = (req, res) => {
    console.log("Get Drinks by Id")
    //B1:  thu thập dữ liệu
    let id = req.params.drinkId;

    //B2:  kiểm tra thông tin
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            message: "Id is invalid!",
        })
    }

    // B3: xủ lý kêt quả
    drinkModel.findById(id)
        .then(data => {
            return res.status(200).json({
                message: 'get Drink by id sucessfully',
                data
            })
        })
        .catch(err => {
            return res.status(404).json({
                status: "Not found ",
                data
            })
        })
}

//Create a Drink
const createDrink = async (req, res) => {
    console.log("Create a Drinks")
    //B1:  thu thập dữ liệu
    let body = req.body
    let id = req.params.drinkId;
    //B2:  kiểm tra thông tin

    if (!mongoose.Types.ObjectId.isValid(id) && id === undefined) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }
    if (!body.maNuocUong) {
        return res.status(400).json({
            message: `maNuocUong is required`
        })
    }
    if (!body.tenNuocUong) {
        return res.status(400).json({
            message: `maNuocUong is required`
        })
    }
    if (!Number.isInteger(body.donGia) || body.donGia < 0) {
        return res.status(400).json({
            message: `student is invalid`
        })
    }
    // B3: xủ lý kêt quả
    try {

        let newDrink = {
            _id: new mongoose.Types.ObjectId(),
            maNuocUong: body.maNuocUong.toUpperCase(),
            tenNuocUong: body.tenNuocUong,
            donGia: body.donGia
        }

        const createDrink = await drinkModel.create(newDrink);
        const updateOrder = await orderModel.findByIdAndUpdate(id,
            {
                $push: { drink: createDrink._id }
            })

        return res.status(201).json({
            message: "Create new Drink sucessfully",
            drink: createDrink,
            order: updateOrder
        })

    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
//// Update a Drink
const updateDrink = (req, res) => {
    // B1 : thu thập dữ liêu
    let id = req.params.drinkId;
    let body = req.body
    // B2 : Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            message: 'Id is invalid!'
        })
    }
    if (!body.tenNuocUong) {
        return res.status(400).json({
            message: `maNuocUong is required`
        })
    }
    if (!Number.isInteger(body.donGia) || body.donGia < 0) {
        return res.status(400).json({
            message: `student is invalid`
        })
    }
    // B3: Xử lý kêt quả
    let newDrink = {
        // _id: new mongoose.Types.ObjectId(),
        maNuocUong: body.tenNuocUong.toUpperCase(),
        tenNuocUong: body.tenNuocUong,
        donGia: body.donGia
    }

    drinkModel.findByIdAndUpdate(id, newDrink)
        .then((data) => {
            return res.status(201).json({
                message: "Update Drink sucessfully",
                course: data
            })
        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })

}

const deleteDrink = (req, res) => {
    // B1 : thu thập dữ liêu
    let id = req.params.drinkId;

    // B2 : Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            message: 'Id is invalid!'
        })
    }
    // xử lý kết quả
    drinkModel.findByIdAndDelete(id)
        .then((data) => {
            return res.status(201).json({
                message: "Create new course sucessfully",
                course: data
            })
        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })
}

module.exports = { getAllDrink, createDrink, getDrinkById, updateDrink, deleteDrink }