const express = require("express");
const { createUser, getAllUser, getUserById, updateUserById, deleteUserById, getAllUserLimt, getAllSkipLimt, getAllSortSkipLimt } = require("../controllers/controllerUser");

const router = express.Router();

router.post("/user",createUser)

router.get("/user", getAllUser)

router.get("/user/:userId", getUserById)

router.put("/user/:userId", updateUserById);

router.delete("/user/:userId", deleteUserById)
//-------------------------------
router.get("/limit-users", getAllUserLimt)

//skip-limit-users
router.get("/skip-limit-users", getAllSkipLimt)

//sort-skip-limit-users
router.get("/sort-skip-limit-users", getAllSortSkipLimt)



module.exports = router