const express = require("express");
const { getAllVoucher, createVoucher, getVoucherById, updateVoucher, deleteVoucher } = require("../controllers/controllerVoucher");

const routerVoucher = express.Router();

routerVoucher.use(express.json());

// get all voucher
routerVoucher.get("/voucher", getAllVoucher);

// get voucher by id
routerVoucher.get("/voucher/:voucherId", getVoucherById);

// Create voucher
routerVoucher.post("/voucher", createVoucher ); 

// Update voucher
routerVoucher.put("/voucher/:voucherId", updateVoucher ); 

// Delete voucher
routerVoucher.delete("/voucher/:voucherId", deleteVoucher ); 


module.exports = routerVoucher