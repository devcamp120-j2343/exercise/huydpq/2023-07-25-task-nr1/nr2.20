// khai báo thư viên mongoose

const mongoose = require("mongoose");
const randtoken = require('rand-token');

const Schema = mongoose.Schema;

const voucherSchema = new Schema({
    _id: Schema.Types.ObjectId,
    maVoucher: {
        type: String, 
       
        unique: true,
         required: true
        },
	phanTramGiamGia: {
        type: Number, 
        required: true
    },
	ghiChu: {
        type: String, 
        required: false}

})

module.exports = mongoose.model("Voucher", voucherSchema)