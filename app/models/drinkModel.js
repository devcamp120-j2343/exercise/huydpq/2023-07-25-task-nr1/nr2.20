// khai bao thu viện mongoose

const mongoose = require("mongoose")

const Schema = mongoose.Schema;

const drinkSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    maNuocUong: {
        type: String,
        unique: true,
        required: true
    },
    tenNuocUong: {
        type: String, 
        required: true
    },
    donGia: {
        type: Number,
        require: true
    }
})
module.exports = mongoose.model("Drink", drinkSchema)